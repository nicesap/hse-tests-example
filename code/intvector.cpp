#include "intvector.h"

#include <cmath>
#include <cassert>
#include <cstdint>
#include <cstring>

#include <algorithm>

vector::vector()
    : vector(10)
{
}

vector::vector(const size_t capacity)
    : mSize{0}
    , mCapacity{0}
    , mData{nullptr}
{
    reserve(capacity);
}

vector::~vector()
{
    free(mData);
}

void vector::set(const size_t index, const int value)
{
    assert(index < size());

    mData[index] = value;
}

int vector::get(const size_t index) const
{
    return mData[index];
}

size_t vector::size() const
{
    return mSize;
}

size_t vector::capacity() const
{
    return mCapacity;
}

void vector::push_back(const int value)
{
    assert(size() < SIZE_MAX / sizeof(int));

    reserve(size() + 1);
    set(mSize++, value);
}

void vector::reserve(const size_t capacity)
{
    using std::max;
    using std::min;

    assert(capacity <= SIZE_MAX / sizeof(int));
    if (mCapacity >= capacity) return;

    size_t newCapacity = max(size_t{1}, mCapacity);
    while (newCapacity < capacity)
    {
        const size_t toAdd = max(size_t{1}, min(newCapacity / 2, SIZE_MAX / sizeof(int) - newCapacity));
        newCapacity += toAdd;
    }

    int* data = reinterpret_cast<int*>(malloc(newCapacity * sizeof(int)));
    assert(data);

    if (mData)
    {
        memcpy(reinterpret_cast<void*>(data), reinterpret_cast<const void*>(mData), mSize * sizeof(int));
        free(mData);
    }

    mData = data;
    mCapacity = newCapacity;
}

bool vector::empty() const
{
    return size() == 0;
}

int vector::front() const
{
    assert(not empty());
    return *mData;
}

int vector::back() const
{
    assert(not empty());
    return mData[size() - 1];
}

const int* vector::data() const
{
    return mData;
}

int* vector::data()
{
    return mData;
}

void vector::insert(const size_t index, const int value)
{
    push_back(value);
    for (size_t i = size() - 1; i != index; --i)
    {
        mData[i] = mData[i - 1];
    }
    mData[index] = value;
}

void vector::erase(const size_t index)
{
    assert(index < mSize);

    for (size_t i = index; i != mSize; ++i)
    {
        i[mData] = mData[i + 1];
    }

    pop_back();
}

void vector::pop_back()
{
    assert(not empty());
    --mSize;
}

void vector::assign(const size_t count, const int value)
{
    assert(count < size());
    for (size_t i = 0; i < count; ++i)
    {
        mData[i] = value;
    }
}

void vector::clear()
{
    mSize = 0;
}

void vector::shrink_to_fit(const size_t capacity)
{
    if (mCapacity <= capacity)
    {
        return;
    }

    if (mSize > capacity)
    {
        mSize = capacity;
    }

    mData = reinterpret_cast<int*>(realloc(reinterpret_cast<void*>(mData), capacity * sizeof(int)));
    mCapacity = capacity;
}

void vector::swap(vector& other)
{
    using std::swap;

    swap(mSize, other.mSize);
    swap(mCapacity, other.mCapacity);
    swap(mData, other.mData);
}

vector& vector::operator=(const vector& other)
{
    if (mSize >= other.mSize)
    {
        const bool needCopy = mSize > 0;
        mSize = other.mSize;
        mCapacity = other.mCapacity;
        if (needCopy)
        {
            memcpy(reinterpret_cast<void*>(mData), reinterpret_cast<const void*>(other.mData), mSize * sizeof(int));
        }
        return *this;
    }

    free(mData);
    mSize = other.mSize;
    mCapacity = other.mCapacity;

    assert(mCapacity > 0);
    assert(other.mData);
    int* data = reinterpret_cast<int*>(malloc(mCapacity * sizeof(int)));
    assert(data);

    mData = data;
    memcpy(reinterpret_cast<void*>(mData), reinterpret_cast<const void*>(other.mData), mSize * sizeof(int));

    return *this;
}

vector::vector(const vector& other)
    : mSize{0}
    , mCapacity{0}
    , mData{nullptr}
{
    *this = other;
}

bool operator>(const vector& x, const vector& y)
{
    const size_t size = std::min(x.size(), y.size());
    for (size_t i = 0; i < size; ++i)
    {
        const int xi = x.get(i);
        const int yi = y.get(i);

        if (xi != yi)
        {
            return xi > yi;
        }
    }

    return x.size() > y.size();
}

bool operator>=(const vector& x, const vector& y)
{
    return not (x < y);
}

bool operator==(const vector& x, const vector& y)
{
    return not (x > y) && not (y > x);
}

bool operator!=(const vector& x, const vector& y)
{
    return not (x == y);
}

bool operator<=(const vector& x, const vector& y)
{
    return y >= x;
}

bool operator<(const vector& x, const vector& y)
{
    return y > x;
}


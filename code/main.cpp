#include "intvector.h"
#include <iostream>
#include <cassert>
#include <unistd.h>

void print(const vector& t)
{
    for (size_t i = 0; i < t.size(); ++i)
    {
        printf("%d ", t.get(i));
    }
    printf("\n");
}

int main()
{
// example of static_cast working not so well for user
//    vector t = static_cast<vector>(-1);

    for (int i = -10; i <= 10; ++i)
    {
        t.push_back(i);
    }

    print(t);

    vector w{t};

    t.shrink_to_fit(5);
    print(t);

    assert(t < w);

    using std::swap;
    swap(t, w);

    assert(t >= w);

// example of memory allocation which does not lead to large resident
// size because of virtialization
//    w.reserve(1<<28);
//    usleep(1e8);

    return 0;
}

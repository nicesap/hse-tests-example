#pragma once

#include <cstddef>

class vector final
{
public:
    explicit vector();
    explicit vector(size_t capacity);

    ~vector();

    void set(size_t index, int value);
    int get(size_t index) const;

    size_t size() const;
    size_t capacity() const;

    void push_back(int value);
    void reserve(size_t capacity);

    // bonus 1
    bool empty() const;
    int front() const;
    int back() const;
    const int* data() const;
    int* data();

    void insert(size_t index, int value);
    void erase(size_t index);

    void pop_back();
    void assign(size_t count, int value);
    void clear();

    // bonus 2
    void shrink_to_fit(size_t capacity);
    void swap(vector& other);
    vector& operator=(const vector& other);
    vector(const vector& other);

private:
    size_t mSize;
    size_t mCapacity;
    int* mData;
};

bool operator>(const vector& x, const vector& y);
bool operator>=(const vector& x, const vector& y);
bool operator==(const vector& x, const vector& y);
bool operator!=(const vector& x, const vector& y);
bool operator<=(const vector& x, const vector& y);
bool operator<(const vector& x, const vector& y);


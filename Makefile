.PHONY: clean

target/vector: code/main.cpp code/intvector.h code/intvector.cpp Makefile | target
	g++ code/main.cpp code/intvector.cpp --std=c++14 -o target/vector -g -fsanitize=address

target:
	mkdir target

clean:
	rm -rf target
